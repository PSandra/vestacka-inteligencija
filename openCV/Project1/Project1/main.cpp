////#include <opencv2/imgproc.hpp>
////#include <opencv2/highgui.hpp>
////#include <opencv2/dnn.hpp>
////using namespace cv;
////using namespace cv::dnn;
////const char* keys =
////"{ help  h     | | Print help message. }"
////"{ input i     | | Path to input image or video file. Skip this argument to capture frames from a camera.}"
////"{ model m     | | Path to a binary .pb file contains trained network.}"
////"{ width       | 320 | Preprocess input image by resizing to a specific width. It should be multiple by 32. }"
////"{ height      | 320 | Preprocess input image by resizing to a specific height. It should be multiple by 32. }"
////"{ thr         | 0.5 | Confidence threshold. }"
////"{ nms         | 0.4 | Non-maximum suppression threshold. }";
////void decode(const Mat& scores, const Mat& geometry, float scoreThresh,
////	std::vector<RotatedRect>& detections, std::vector<float>& confidences);
////int main(int argc, char** argv)
////{
////	// Parse command line arguments.
////	CommandLineParser parser(argc, argv, keys);
////	parser.about("Use this script to run TensorFlow implementation (https://github.com/argman/EAST) of "
////		"EAST: An Efficient and Accurate Scene Text Detector (https://arxiv.org/abs/1704.03155v2)");
////	if (argc == 1 || parser.has("help"))
////	{
////		parser.printMessage();
////		return 0;
////	}
////	float confThreshold = parser.get<float>("thr");
////	float nmsThreshold = parser.get<float>("nms");
////	int inpWidth = parser.get<int>("width");
////	int inpHeight = parser.get<int>("height");
////	String model = parser.get<String>("model");
////	if (!parser.check())
////	{
////		parser.printErrors();
////		return 1;
////	}
////	CV_Assert(!model.empty());
////	// Load network.
////	Net net = readNet(model);
////	// Open a video file or an image file or a camera stream.
////	VideoCapture cap;
////	if (parser.has("input"))
////		cap.open(parser.get<String>("input"));
////	else
////		cap.open(0);
////	static const std::string kWinName = "EAST: An Efficient and Accurate Scene Text Detector";
////	namedWindow(kWinName, WINDOW_NORMAL);
////	std::vector<Mat> outs;
////	std::vector<String> outNames(2);
////	outNames[0] = "feature_fusion/Conv_7/Sigmoid";
////	outNames[1] = "feature_fusion/concat_3";
////	Mat frame, blob;
////	while (waitKey(1) < 0)
////	{
////		cap >> frame;
////		if (frame.empty())
////		{
////			waitKey();
////			break;
////		}
////		blobFromImage(frame, blob, 1.0, Size(inpWidth, inpHeight), Scalar(123.68, 116.78, 103.94), true, false);
////		net.setInput(blob);
////		net.forward(outs, outNames);
////		Mat scores = outs[0];
////		Mat geometry = outs[1];
////		// Decode predicted bounding boxes.
////		std::vector<RotatedRect> boxes;
////		std::vector<float> confidences;
////		decode(scores, geometry, confThreshold, boxes, confidences);
////		// Apply non-maximum suppression procedure.
////		std::vector<int> indices;
////		NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
////		// Render detections.
////		Point2f ratio((float)frame.cols / inpWidth, (float)frame.rows / inpHeight);
////		for (size_t i = 0; i < indices.size(); ++i)
////		{
////			RotatedRect& box = boxes[indices[i]];
////			Point2f vertices[4];
////			box.points(vertices);
////			for (int j = 0; j < 4; ++j)
////			{
////				vertices[j].x *= ratio.x;
////				vertices[j].y *= ratio.y;
////			}
////			for (int j = 0; j < 4; ++j)
////				line(frame, vertices[j], vertices[(j + 1) % 4], Scalar(0, 255, 0), 1);
////		}
////		// Put efficiency information.
////		std::vector<double> layersTimes;
////		double freq = getTickFrequency() / 1000;
////		double t = net.getPerfProfile(layersTimes) / freq;
////		std::string label = format("Inference time: %.2f ms", t);
////		putText(frame, label, Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0));
////		imshow(kWinName, frame);
////	}
////	return 0;
////}
////void decode(const Mat& scores, const Mat& geometry, float scoreThresh,
////	std::vector<RotatedRect>& detections, std::vector<float>& confidences)
////{
////	detections.clear();
////	CV_Assert(scores.dims == 4); CV_Assert(geometry.dims == 4); CV_Assert(scores.size[0] == 1);
////	CV_Assert(geometry.size[0] == 1); CV_Assert(scores.size[1] == 1); CV_Assert(geometry.size[1] == 5);
////	CV_Assert(scores.size[2] == geometry.size[2]); CV_Assert(scores.size[3] == geometry.size[3]);
////	const int height = scores.size[2];
////	const int width = scores.size[3];
////	for (int y = 0; y < height; ++y)
////	{
////		const float* scoresData = scores.ptr<float>(0, 0, y);
////		const float* x0_data = geometry.ptr<float>(0, 0, y);
////		const float* x1_data = geometry.ptr<float>(0, 1, y);
////		const float* x2_data = geometry.ptr<float>(0, 2, y);
////		const float* x3_data = geometry.ptr<float>(0, 3, y);
////		const float* anglesData = geometry.ptr<float>(0, 4, y);
////		for (int x = 0; x < width; ++x)
////		{
////			float score = scoresData[x];
////			if (score < scoreThresh)
////				continue;
////			// Decode a prediction.
////			// Multiple by 4 because feature maps are 4 time less than input image.
////			float offsetX = x * 4.0f, offsetY = y * 4.0f;
////			float angle = anglesData[x];
////			float cosA = std::cos(angle);
////			float sinA = std::sin(angle);
////			float h = x0_data[x] + x2_data[x];
////			float w = x1_data[x] + x3_data[x];
////			Point2f offset(offsetX + cosA * x1_data[x] + sinA * x2_data[x],
////				offsetY - sinA * x1_data[x] + cosA * x2_data[x]);
////			Point2f p1 = Point2f(-sinA * h, -cosA * h) + offset;
////			Point2f p3 = Point2f(-cosA * w, sinA * w) + offset;
////			RotatedRect r(0.5f * (p1 + p3), Size2f(w, h), -angle * 180.0f / (float)CV_PI);
////			detections.push_back(r);
////			confidences.push_back(score);
////		}
////	}
////}
//
//// GenData.cpp
//
//#include<opencv2/core/core.hpp>
//#include<opencv2/highgui/highgui.hpp>
//#include<opencv2/imgproc/imgproc.hpp>
//#include<opencv2/ml/ml.hpp>
//
//#include<iostream>
//#include<vector>
//
//// global variables ///////////////////////////////////////////////////////////////////////////////
//const int MIN_CONTOUR_AREA = 100;
//
//const int RESIZED_IMAGE_WIDTH = 20;
//const int RESIZED_IMAGE_HEIGHT = 30;
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
//int main() {
//
//	cv::Mat imgTrainingNumbers;         // input image
//	cv::Mat imgGrayscale;               // 
//	cv::Mat imgBlurred;                 // declare various images
//	cv::Mat imgThresh;                  //
//	cv::Mat imgThreshCopy;              //
//
//	std::vector<std::vector<cv::Point> > ptContours;        // declare contours vector
//	std::vector<cv::Vec4i> v4iHierarchy;                    // declare contours hierarchy
//
//	cv::Mat matClassificationInts;      // these are our training classifications, note we will have to perform some conversions before writing to file later
//
//										// these are our training images, due to the data types that the KNN object KNearest requires, we have to declare a single Mat,
//										// then append to it as though it's a vector, also we will have to perform some conversions before writing to file later
//	cv::Mat matTrainingImagesAsFlattenedFloats;
//
//	// possible chars we are interested in are digits 0 through 9 and capital letters A through Z, put these in vector intValidChars
//	std::vector<int> intValidChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
//		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
//		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
//		'U', 'V', 'W', 'X', 'Y', 'Z' };
//
//	imgTrainingNumbers = cv::imread("training_chars2.png");          // read in training numbers image
//
//	if (imgTrainingNumbers.empty()) {                               // if unable to open image
//		std::cout << "error: image not read from file\n\n";         // show error message on command line
//		return(0);                                                  // and exit program
//	}
//	//CV_BGR2GRAY
//
//	cv::cvtColor(imgTrainingNumbers, imgGrayscale, cv::COLOR_BGR2GRAY);        // convert to grayscale
//
//	cv::GaussianBlur(imgGrayscale,              // input image
//		imgBlurred,                             // output image
//		cv::Size(5, 5),                         // smoothing window width and height in pixels
//		0);                                     // sigma value, determines how much the image will be blurred, zero makes function choose the sigma value
//
//												// filter image from grayscale to black and white
//	cv::adaptiveThreshold(imgBlurred,           // input image
//		imgThresh,                              // output image
//		255,                                    // make pixels that pass the threshold full white
//		cv::ADAPTIVE_THRESH_GAUSSIAN_C,         // use gaussian rather than mean, seems to give better results
//		cv::THRESH_BINARY_INV,                  // invert so foreground will be white, background will be black
//		11,                                     // size of a pixel neighborhood used to calculate threshold value
//		2);                                     // constant subtracted from the mean or weighted mean
//
//	cv::imshow("imgThresh", imgThresh);         // show threshold image for reference
//
//	imgThreshCopy = imgThresh.clone();          // make a copy of the thresh image, this in necessary b/c findContours modifies the image
//
//	cv::findContours(imgThreshCopy,             // input image, make sure to use a copy since the function will modify this image in the course of finding contours
//		ptContours,                             // output contours
//		v4iHierarchy,                           // output hierarchy
//		cv::RETR_EXTERNAL,                      // retrieve the outermost contours only
//		cv::CHAIN_APPROX_SIMPLE);               // compress horizontal, vertical, and diagonal segments and leave only their end points
//
//	for (int i = 0; i < ptContours.size(); i++) {                           // for each contour
//		if (cv::contourArea(ptContours[i]) > MIN_CONTOUR_AREA) {                // if contour is big enough to consider
//			cv::Rect boundingRect = cv::boundingRect(ptContours[i]);                // get the bounding rect
//
//			cv::rectangle(imgTrainingNumbers, boundingRect, cv::Scalar(0, 0, 255), 2);      // draw red rectangle around each contour as we ask user for input
//
//			cv::Mat matROI = imgThresh(boundingRect);           // get ROI image of bounding rect
//
//			cv::Mat matROIResized;
//			cv::resize(matROI, matROIResized, cv::Size(RESIZED_IMAGE_WIDTH, RESIZED_IMAGE_HEIGHT));     // resize image, this will be more consistent for recognition and storage
//
//			cv::imshow("matROI", matROI);                               // show ROI image for reference
//			cv::imshow("matROIResized", matROIResized);                 // show resized ROI image for reference
//			cv::imshow("imgTrainingNumbers", imgTrainingNumbers);       // show training numbers image, this will now have red rectangles drawn on it
//
//			int intChar = cv::waitKey(0);           // get key press
//
//			if (intChar == 27) {        // if esc key was pressed
//				return(0);              // exit program
//			}
//			else if (std::find(intValidChars.begin(), intValidChars.end(), intChar) != intValidChars.end()) {     // else if the char is in the list of chars we are looking for . . .
//
//				matClassificationInts.push_back(intChar);       // append classification char to integer list of chars
//
//				cv::Mat matImageFloat;                          // now add the training image (some conversion is necessary first) . . .
//				matROIResized.convertTo(matImageFloat, CV_32FC1);       // convert Mat to float
//
//				cv::Mat matImageFlattenedFloat = matImageFloat.reshape(1, 1);       // flatten
//
//				matTrainingImagesAsFlattenedFloats.push_back(matImageFlattenedFloat);       // add to Mat as though it was a vector, this is necessary due to the
//																							// data types that KNearest.train accepts
//			}   // end if
//		}   // end if
//	}   // end for
//
//	std::cout << "training complete\n\n";
//
//	// save classifications to file ///////////////////////////////////////////////////////
//
//	cv::FileStorage fsClassifications("classifications.xml", cv::FileStorage::WRITE);           // open the classifications file
//
//	if (fsClassifications.isOpened() == false) {                                                        // if the file was not opened successfully
//		std::cout << "error, unable to open training classifications file, exiting program\n\n";        // show error message
//		return(0);                                                                                      // and exit program
//	}
//
//	fsClassifications << "classifications" << matClassificationInts;        // write classifications into classifications section of classifications file
//	fsClassifications.release();                                            // close the classifications file
//
//																			// save training images to file ///////////////////////////////////////////////////////
//
//	cv::FileStorage fsTrainingImages("images.xml", cv::FileStorage::WRITE);         // open the training images file
//
//	if (fsTrainingImages.isOpened() == false) {                                                 // if the file was not opened successfully
//		std::cout << "error, unable to open training images file, exiting program\n\n";         // show error message
//		return(0);                                                                              // and exit program
//	}
//
//	fsTrainingImages << "images" << matTrainingImagesAsFlattenedFloats;         // write training images into images section of images file
//	fsTrainingImages.release();                                                 // close the training images file
//
//	return(0);
//}
//
//
//
//
// TrainAndTest.cpp

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/ml/ml.hpp>

#include<iostream>
#include<sstream>

// global variables ///////////////////////////////////////////////////////////////////////////////
const int MIN_CONTOUR_AREA = 100;

const int RESIZED_IMAGE_WIDTH = 20;
const int RESIZED_IMAGE_HEIGHT = 30;

///////////////////////////////////////////////////////////////////////////////////////////////////
class ContourWithData {
public:
	// member variables ///////////////////////////////////////////////////////////////////////////
	std::vector<cv::Point> ptContour;           // contour
	cv::Rect boundingRect;                      // bounding rect for contour
	float fltArea;                              // area of contour

												///////////////////////////////////////////////////////////////////////////////////////////////
	bool checkIfContourIsValid() {                              // obviously in a production grade program
		if (fltArea < MIN_CONTOUR_AREA) return false;           // we would have a much more robust function for 
		return true;                                            // identifying if a contour is valid !!
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	static bool sortByBoundingRectXPosition(const ContourWithData& cwdLeft, const ContourWithData& cwdRight) {      // this function allows us to sort
		return(cwdLeft.boundingRect.x < cwdRight.boundingRect.x);                                                   // the contours from left to right
	}

};

///////////////////////////////////////////////////////////////////////////////////////////////////
int main() {
	std::vector<ContourWithData> allContoursWithData;           // declare empty vectors,
	std::vector<ContourWithData> validContoursWithData;         // we will fill these shortly

																// read in training classifications ///////////////////////////////////////////////////

	cv::Mat matClassificationInts;      // we will read the classification numbers into this variable as though it is a vector

	cv::FileStorage fsClassifications("classifications.xml", cv::FileStorage::READ);        // open the classifications file

	if (fsClassifications.isOpened() == false) {                                                    // if the file was not opened successfully
		std::cout << "error, unable to open training classifications file, exiting program\n\n";    // show error message
		return(0);                                                                                  // and exit program
	}

	fsClassifications["classifications"] >> matClassificationInts;      // read classifications section into Mat classifications variable
	fsClassifications.release();                                        // close the classifications file

																		// read in training images ////////////////////////////////////////////////////////////

	cv::Mat matTrainingImagesAsFlattenedFloats;         // we will read multiple images into this single image variable as though it is a vector

	cv::FileStorage fsTrainingImages("images.xml", cv::FileStorage::READ);          // open the training images file

	if (fsTrainingImages.isOpened() == false) {                                                 // if the file was not opened successfully
		std::cout << "error, unable to open training images file, exiting program\n\n";         // show error message
		return(0);                                                                              // and exit program
	}

	fsTrainingImages["images"] >> matTrainingImagesAsFlattenedFloats;           // read images section into Mat training images variable
	fsTrainingImages.release();                                              // close the traning images file

																				// train //////////////////////////////////////////////////////////////////////////////

	cv::Ptr<cv::ml::KNearest>  kNearest(cv::ml::KNearest::create());            // instantiate the KNN object

																				// finally we get to the call to train, note that both parameters have to be of type Mat (a single Mat)
																				// even though in reality they are multiple images / numbers
	kNearest->train(matTrainingImagesAsFlattenedFloats, cv::ml::ROW_SAMPLE, matClassificationInts);

	// test ///////////////////////////////////////////////////////////////////////////////

	cv::Mat matTestingNumbers = cv::imread("test1.png");            // read in the test numbers image

	if (matTestingNumbers.empty()) {                                // if unable to open image
		std::cout << "error: image not read from file\n\n";         // show error message on command line
		return(0);                                                  // and exit program
	}

	cv::Mat matGrayscale;           //
	cv::Mat matBlurred;             // declare more image variables
	cv::Mat matThresh;              //
	cv::Mat matThreshCopy;          //

	cv::cvtColor(matTestingNumbers, matGrayscale, cv::COLOR_BGR2GRAY);         // convert to grayscale

																		// blur
	cv::GaussianBlur(matGrayscale,  // input image
		matBlurred,                // output image
		cv::Size(5, 5),            // smoothing window width and height in pixels
		0);                        // sigma value, determines how much the image will be blurred, zero makes function choose the sigma value

								   // filter image from grayscale to black and white
	cv::adaptiveThreshold(matBlurred,                           // input image
		matThresh,                            // output image
		255,                                  // make pixels that pass the threshold full white
		cv::ADAPTIVE_THRESH_GAUSSIAN_C,       // use gaussian rather than mean, seems to give better results
		cv::THRESH_BINARY_INV,                // invert so foreground will be white, background will be black
		11,                                   // size of a pixel neighborhood used to calculate threshold value
		2);                                   // constant subtracted from the mean or weighted mean

	matThreshCopy = matThresh.clone();              // make a copy of the thresh image, this in necessary b/c findContours modifies the image

	std::vector<std::vector<cv::Point> > ptContours;        // declare a vector for the contours
	std::vector<cv::Vec4i> v4iHierarchy;                    // declare a vector for the hierarchy (we won't use this in this program but this may be helpful for reference)

	cv::findContours(matThreshCopy,             // input image, make sure to use a copy since the function will modify this image in the course of finding contours
		ptContours,                             // output contours
		v4iHierarchy,                           // output hierarchy
		cv::RETR_EXTERNAL,                      // retrieve the outermost contours only
		cv::CHAIN_APPROX_SIMPLE);               // compress horizontal, vertical, and diagonal segments and leave only their end points

	for (int i = 0; i < ptContours.size(); i++) {               // for each contour
		ContourWithData contourWithData;                                                    // instantiate a contour with data object
		contourWithData.ptContour = ptContours[i];                                          // assign contour to contour with data
		contourWithData.boundingRect = cv::boundingRect(contourWithData.ptContour);         // get the bounding rect
		contourWithData.fltArea = cv::contourArea(contourWithData.ptContour);               // calculate the contour area
		allContoursWithData.push_back(contourWithData);                                     // add contour with data object to list of all contours with data
	}

	for (int i = 0; i < allContoursWithData.size(); i++) {                      // for all contours
		if (allContoursWithData[i].checkIfContourIsValid()) {                   // check if valid
			validContoursWithData.push_back(allContoursWithData[i]);            // if so, append to valid contour list
		}
	}
	// sort contours from left to right
	std::sort(validContoursWithData.begin(), validContoursWithData.end(), ContourWithData::sortByBoundingRectXPosition);

	std::string strFinalString;         // declare final string, this will have the final number sequence by the end of the program

	for (int i = 0; i < validContoursWithData.size(); i++) {            // for each contour

																		// draw a green rect around the current char
		cv::rectangle(matTestingNumbers,                            // draw rectangle on original image
			validContoursWithData[i].boundingRect,        // rect to draw
			cv::Scalar(0, 255, 0),                        // green
			2);                                           // thickness

		cv::Mat matROI = matThresh(validContoursWithData[i].boundingRect);          // get ROI image of bounding rect

		cv::Mat matROIResized;
		cv::resize(matROI, matROIResized, cv::Size(RESIZED_IMAGE_WIDTH, RESIZED_IMAGE_HEIGHT));     // resize image, this will be more consistent for recognition and storage

		cv::Mat matROIFloat;
		matROIResized.convertTo(matROIFloat, CV_32FC1);             // convert Mat to float, necessary for call to find_nearest

		cv::Mat matROIFlattenedFloat = matROIFloat.reshape(1, 1);

		cv::Mat matCurrentChar(0, 0, CV_32F);

		kNearest->findNearest(matROIFlattenedFloat, 1, matCurrentChar);     // finally we can call find_nearest !!!

		float fltCurrentChar = (float)matCurrentChar.at<float>(0, 0);

		strFinalString = strFinalString + char(int(fltCurrentChar));        // append current char to full string
	}

	std::cout << "\n\n" << "Tekst: = " << strFinalString << "\n\n";       // show the full string

	cv::imshow("matTestingNumbers", matTestingNumbers);     // show input image with green boxes drawn around found digits

	cv::waitKey(0);                                         // wait for user key press

	return(0);
}
